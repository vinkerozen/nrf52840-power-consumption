/*
 * Copyright (c) 2016 Intel Corporation
 *
 * SPDX-License-Identifier: Apache-2.0
 */

#include <zephyr/kernel.h>
#include <zephyr/drivers/gpio.h>
#include <zephyr/device.h>
#include <zephyr/devicetree.h>
#include <zephyr/drivers/gpio.h>
#include <zephyr/pm/device.h>
#include <zephyr/logging/log.h>
LOG_MODULE_REGISTER(Power);


/* 1000 msec = 1 sec */
#define SLEEP_TIME_MS   5000

/* The devicetree node identifier for the "led0" alias. */
#define LED0_NODE DT_ALIAS(led0)

static const struct gpio_dt_spec led = GPIO_DT_SPEC_GET(LED0_NODE, gpios);
static const struct gpio_dt_spec pinIT = GPIO_DT_SPEC_GET(DT_ALIAS(aclit), gpios);
   

volatile bool itTriggered = false;

// actual function that will be call after a rising edge on pin
void accelerometer_interrupt_callback(const struct device *dev, struct gpio_callback *cb, uint32_t pins) {
	gpio_pin_toggle_dt(&led);
	//gpio_pin_configure_dt(&pinIT, GPIO_DISCONNECTED);
	//gpio_pin_configure_dt(&pinIT, GPIO_INPUT);
	//gpio_pin_interrupt_configure_dt(&pinIT, GPIO_INT_LEVEL_INACTIVE);
	//gpio_pin_interrupt_configure_dt(&pinIT, GPIO_INT_LEVEL_HIGH);
	itTriggered = true;
}

void acl_interrupt() {
	  	
	// check if the pin is ready
	if(!device_is_ready(pinIT.port)) {
		LOG_ERR("Device is not ready !");
		return;
	}

	// configure pin gpio as input
	int ret = gpio_pin_configure_dt(&pinIT, GPIO_INPUT);
	if (ret != 0) {
		LOG_ERR("Error %d: failed to configure %s pin %d\n", ret, pinIT.port->name, pinIT.pin);
		return;
	}


	// enable interrupt on pin for rising edge
	ret = gpio_pin_interrupt_configure_dt(&pinIT, GPIO_INT_LEVEL_LOW);
	if (ret != 0) {
		LOG_ERR("Error %d: failed to configure interrupt on %s pin %d\n", ret, pinIT.port->name, pinIT.pin);
		return;
	}

	// initialize and attach callback structure for pin interrupt
	static struct gpio_callback it;
	gpio_init_callback(&it, accelerometer_interrupt_callback, BIT(pinIT.pin));
	gpio_add_callback(pinIT.port, &it);

	
}





int main(void)
{
	
	int ret;
	if (!gpio_is_ready_dt(&led)) {
		return 0;
	}

	ret = gpio_pin_configure_dt(&led, GPIO_OUTPUT_INACTIVE);
	if (ret < 0) {
		return 0;
	}

	acl_interrupt();

	while (1) {
		
		

		if(itTriggered){
			//k_msleep(20);
			//gpio_pin_configure_dt(&pinIT, GPIO_INPUT);
			//gpio_pin_interrupt_configure_dt(&pinIT, GPIO_INT_LEVEL_HIGH);
			//acl_interrupt();
			itTriggered = false;
		} else {
			k_msleep(SLEEP_TIME_MS);
		}
	
	}
	return 0;
}
