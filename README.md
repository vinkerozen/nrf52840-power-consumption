# SOLSIUS   

NRF52 micro firmware for power consumption tests

## dev board

PCA10040 modified for current testing.  
SOC NRF52832.

## bench tests

Interrupt high level on PORT event use very little power at idle but the wake-up is incredibly more power consumer.


#### INTERRUPT ON PIN:

1. Example with an interrupt on pin at [**IDLE**](screenshot/interrupt_rising_edge_on_pin_idle.png)
2. Example with an interrupt on pin at [**TRIGGER**](screenshot/interrupt_rising_edge_on_pin_trigger.png)

#### INTERRUPT ON PORT:

1. Example with an interrupt on port at [**IDLE**](screenshot/interrupt_high_level_on_port_idle.png)
2. Example with an interrupt on port at [**TRIGGER**](screenshot/interrupt_high_level_on_port_trigger.png)   
   
#### Errata 97

The issue with high current in idle for interrupt on pin has an [errata 97](https://infocenter.nordicsemi.com/index.jsp?topic=%2Ferrata_nRF52832_Rev2%2FERR%2FnRF52832%2FRev2%2Flatest%2Fanomaly_832_97.html)


#### Interrupt Port at LOW 

With an interrupt config whit `GPIO_INT_LEVEL_LOW` current burst are [shorter](
screenshot/interrupt_shorter.png)










